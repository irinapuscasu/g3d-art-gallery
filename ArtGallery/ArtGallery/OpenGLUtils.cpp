#include "OpenGLUtils.h"
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "stbi_image_write.h"

namespace OpenGLUtils {

	void framebuffer_size_callback(GLFWwindow* window, int width, int height)
	{
		// make sure the viewport matches the new window dimensions; note that width and 
		// height will be significantly larger than specified on retina displays.
		glViewport(0, 0, width, height);
	}
	GLFWwindow* initializeWindow(unsigned width, unsigned height, const std::string& title)
	{
		if (!glfwInit()) {
			fprintf(stderr, "failed");
			getchar();
			return nullptr;
		}
		GLFWwindow* window;
		glfwWindowHint(GLFW_SAMPLES, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_DEPTH_BITS, GL_TRUE);

		window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
		if (window == nullptr) {
			std::cerr << "Failed to open GLFW window" << std::endl;
			getchar();
			glfwTerminate();
			return nullptr;
		}
		glfwMakeContextCurrent(window);
		glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
		glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
		glfwSetCursorPos(window, width / 2, height / 2);
		if (glewInit() != GLEW_OK) {
			std::cerr << "Failed to initialize GLEW" << std::endl;
			getchar();
			glfwTerminate();
			return nullptr;
		}
		return window;
	}
	GLuint generateTexture2D(const std::string & name, const std::string & directory)
	{
		return generateTexture2D(directory + '\\' + name);
	}
	void setVisibleCursor(GLFWwindow* window, bool visible) {
		if(visible)
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		else
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
	GLuint generateTexture2D(const std::string & path)
	{
		GLuint textureId;
		GLint width, height, nrChannels;
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		unsigned char *data = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);
		GLenum format;
		switch (nrChannels)
		{
		case 1:
			format = GL_RED; break;
		case 3:
			format = GL_RGB; break;
		case 4:
			format = GL_RGBA; break;
		}
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		if (data)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else
		{
			std::cout << "Failed to load texture " << path << std::endl;
		}
		stbi_image_free(data);
		return textureId;
	}
	GLuint generateTextureCubeMap(const std::vector<std::string>& faces) {
			GLuint textureId;
			glGenTextures(1, &textureId);
			glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);
			int width, height, nrChannels;
			for (GLuint i = 0; i < faces.size(); ++i)
			{
				unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
				GLenum format;
				switch (nrChannels)
				{
				case 1:
					format = GL_RED; break;
				case 3:
					format = GL_RGB; break;
				case 4:
					format = GL_RGBA; break;
				}
				glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
				if (data)
					glTexImage2D(
						GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
						0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, data);
				else
					std::cout << "Failed to load texture" << std::endl;
				stbi_image_free(data);
			}
			glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		return textureId;
	}
	void processInput(GLFWwindow * window)
	{
	}
	void flipTexturesVertically(bool flip)
	{
		stbi_set_flip_vertically_on_load(flip);
	}
}

