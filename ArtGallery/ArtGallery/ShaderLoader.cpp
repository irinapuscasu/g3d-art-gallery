#include "ShaderLoader.h"
#include <fstream>
#include <iostream>
#include <sstream>

#include <gtc\type_ptr.hpp>

ShaderLoader::ShaderLoader(const std::string& shaderPath, const std::string& fragmentPath):
	programId(glCreateProgram()), vertexShaderPath(shaderPath), fragmentShaderPath(fragmentPath)
{
	loadShaders();
}

ShaderLoader::~ShaderLoader()
{
}

bool ShaderLoader::loadShaders()
{
	vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
	std::string vertexShaderCode;
	std::ifstream vertexShaderFile(vertexShaderPath, std::ios::in);
	if (!vertexShaderFile.is_open()) {
		std::cerr << "Vertex Shader source file could not be opened" << std::endl;
		return false;
	}
	try {
		std::stringstream vertexShaderStream;
		vertexShaderStream << vertexShaderFile.rdbuf();
		vertexShaderCode = vertexShaderStream.str();
	}
	catch (const std::iostream::failure& e) {
		std::cerr << "Couldn't read from the vertex source file" << std::endl;
		return false;
	}
	const GLchar* vertexShaderCString = vertexShaderCode.c_str();
	glShaderSource(vertexShaderId, 1, &vertexShaderCString, NULL);
	glCompileShader(vertexShaderId);
	GLint success;
	glGetShaderiv(vertexShaderId, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLint errorMessageLength;
		glGetShaderiv(vertexShaderId, GL_INFO_LOG_LENGTH, &errorMessageLength);
		GLchar* errorMessage = new GLchar[errorMessageLength];
		glGetShaderInfoLog(vertexShaderId, errorMessageLength, NULL, errorMessage);
		std::cerr << errorMessage << std::endl;
		delete[] errorMessage;
		return false;
	}


	fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragmentShaderCode;
	std::ifstream fragmentShaderFile(fragmentShaderPath, std::ios::out);
	if (!fragmentShaderFile.is_open()) {
		std::cerr << "Fragment Shader source file could not be opened" << std::endl;
		return false;
	}
	try {
		std::stringstream fragmentShaderStream;
		fragmentShaderStream << fragmentShaderFile.rdbuf();
		fragmentShaderCode = fragmentShaderStream.str();
	}
	catch (const std::ios::failure& e) {
		std::cerr << "Couldn't read from the fragment shader file" << std::endl;
		return false;
	}
	const GLchar* fragmentShaderCString = fragmentShaderCode.c_str();
	glShaderSource(fragmentShaderId, 1, &fragmentShaderCString, NULL);
	glCompileShader(fragmentShaderId);
	glGetShaderiv(fragmentShaderId, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLint errorMessageLength;
		glGetShaderiv(fragmentShaderId, GL_INFO_LOG_LENGTH, &errorMessageLength);
		GLchar* errorMessage = new GLchar[errorMessageLength];
		glGetShaderInfoLog(fragmentShaderId, errorMessageLength, NULL, errorMessage);
		std::cerr << errorMessage << std::endl;
		delete[] errorMessage;
		return false;
	}
	glAttachShader(programId, vertexShaderId);
	glAttachShader(programId, fragmentShaderId);
	glLinkProgram(programId);

	glGetProgramiv(programId, GL_LINK_STATUS, &success);
	if (!success) {
		GLint errorMessageLength;
		glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &errorMessageLength);
		GLchar* errorMessage = new GLchar[errorMessageLength];
		glGetProgramInfoLog(programId, errorMessageLength, NULL, errorMessage);
		delete[] errorMessage;
		return false;
	}

	glDetachShader(programId, vertexShaderId);
	glDetachShader(programId, fragmentShaderId);
	glDeleteShader(vertexShaderId);
	glDeleteShader(fragmentShaderId);

	return true;
}

void ShaderLoader::setInt(const std::string & name, int value)
{
	glUniform1i(glGetUniformLocation(programId, name.c_str()), value);
}

void ShaderLoader::setFloat(const std::string & name, float value)
{
	glUniform1f(glGetUniformLocation(programId, name.c_str()), value);
}

void ShaderLoader::setVec3(const std::string & name, const glm::vec3 & value)
{
	glUniform3fv(glGetUniformLocation(programId, name.c_str()), 1, glm::value_ptr(value));
}

void ShaderLoader::setMat4(const std::string & name, const glm::mat4 & value)
{
	glUniformMatrix4fv(glGetUniformLocation(programId, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
}

void ShaderLoader::useProgram()
{
	glUseProgram(programId);
}
