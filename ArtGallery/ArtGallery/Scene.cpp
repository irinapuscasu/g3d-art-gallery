#include "Scene.h"
#include "GlassSupport.h"
#include "Model.h"
#include <ctime>
#include <gtc\matrix_transform.hpp>
#include <gtx\quaternion.hpp>


void Scene::addObject(const std::string& label, OpenGLObject* obj) {
	objects[label] = obj;
}
void Scene::initialize()
{
	for (std::pair<std::string, OpenGLObject*> obj : objects)
		obj.second->initialize();
}

void Scene::update()
{
	for (std::pair<std::string, OpenGLObject*> obj : objects)
		obj.second->update();
}

void Scene::draw()
{
	for (std::pair<std::string, OpenGLObject*> obj : objects) {
		Model * model = dynamic_cast<Model*>(obj.second);
		if (model) {
			model->getShader().useProgram();
			model->getShader().setFloat("scaleFactor", model->getScaleFactor());
		}
		obj.second->draw();
	}
}
void Scene::addCamera(Camera2 * camera)
{
	if (!cameras.size())
		activeCamera = camera;
	cameras.push_back(camera);
}
Camera2 * Scene::getActiveCamera()
{
	return activeCamera;
}
void Scene::keyboardCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	GlassSupport* a = dynamic_cast<GlassSupport*>(objects["cube"]);
	switch (key)
	{
		long long deltaTime;
	case GLFW_KEY_W:
		deltaTime = getTimeSinceLastToggle();
		if (deltaTime > toggleActivationDelay) {
			if (!wireFrame)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			wireFrame = !wireFrame;
			lastToggleTime += deltaTime;
		}
		break;
	case GLFW_KEY_0:
		activeCamera = cameras[0];
		break;
	case GLFW_KEY_1:
		activeCamera = cameras[1];
		break;
	case GLFW_KEY_2:
		activeCamera = cameras[2];
		break;
	case GLFW_KEY_3:
		activeCamera = cameras[3];
		break;
	case GLFW_KEY_4:
		activeCamera = cameras[4];
		break;

	default:
		activeCamera->processKeyboardInput(key, action);
	}
}

void Scene::mouseScrollCallback(GLFWwindow * window, double x, double y)
{
	activeCamera->updateZoomLevel(y);
}



void Scene::mouseCallback(GLFWwindow * window, double x, double y)
{
	if(activeCamera->getMouseMovementEnabled())
		activeCamera->processMouseMovement(x, y);
}

void Scene::mouseButtonCallback(GLFWwindow * window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT)
		if (action == GLFW_PRESS) {
			activeCamera->setMouseMovementEnabled(true);
			double x, y;
			glfwGetCursorPos(window, &x, &y);
			//camera->prevX = x;
			//camera->prevY = y;
		}
		else if (action == GLFW_RELEASE)
			activeCamera->setMouseMovementEnabled(false);

}

long long Scene::getTimeSinceLastToggle()
{
	return std::chrono::duration_cast< std::chrono::milliseconds >(
		std::chrono::system_clock::now().time_since_epoch() - std::chrono::milliseconds(lastToggleTime)).count();
}

bool Scene::wireFrame = false;
long long Scene::lastToggleTime = 0;
Camera2* Scene::activeCamera = nullptr;
std::vector<Camera2*> Scene::cameras;
std::unordered_map<std::string, OpenGLObject*> Scene::objects;