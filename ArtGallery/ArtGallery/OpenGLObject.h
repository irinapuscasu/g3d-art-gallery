#pragma once

class OpenGLObject
{
public:
	virtual void initialize() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;
};

