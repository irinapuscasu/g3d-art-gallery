#include "GlassSupport.h"
#include "gtc\type_ptr.hpp"
#include "gtc\matrix_transform.hpp"

GlassSupport::GlassSupport(ShaderLoader shader):
shader(shader)
{
}

GlassSupport::~GlassSupport()
{
}

void GlassSupport::initialize()
{
	initializeVertices();
	initializeBuffers();
}

void GlassSupport::initializeVertices()
{
	points = {
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f, 
		1.0f,  1.0f, -1.0f, 
		1.0f,  1.0f, -1.0f, 
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
	
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f, 
		1.0f,  1.0f,  1.0f, 
		1.0f,  1.0f,  1.0f, 
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,
	
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
	
		1.0f,  1.0f,  1.0f, 
		1.0f,  1.0f, -1.0f, 
		1.0f, -1.0f, -1.0f, 
		1.0f, -1.0f, -1.0f, 
		1.0f, -1.0f,  1.0f, 
		1.0f,  1.0f,  1.0f, 
	
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f, 
		1.0f, -1.0f,  1.0f, 
		1.0f, -1.0f,  1.0f, 
		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
	
		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f, 
		1.0f,  1.0f,  1.0f, 
		1.0f,  1.0f,  1.0f, 
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,
	};

	normals = {
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
	
		0.0f,  0.0f,  1.0f,
		0.0f,  0.0f,  1.0f,
		0.0f,  0.0f,  1.0f,
		0.0f,  0.0f,  1.0f,
		0.0f,  0.0f,  1.0f,
		0.0f,  0.0f,  1.0f,
	
		-1.0f,  0.0f,  0.0f,
		-1.0f,  0.0f,  0.0f,
		-1.0f,  0.0f,  0.0f,
		-1.0f,  0.0f,  0.0f,
		-1.0f,  0.0f,  0.0f,
		-1.0f,  0.0f,  0.0f,
	
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
	
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
	
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
	};
}

void GlassSupport::initializeBuffers()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);


	glGenBuffers(1, &pointsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, pointsBuffer);
	glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(float), &points[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &normalsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &normals[0], GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GlassSupport::update()
{
}

void GlassSupport::draw()
{
	shader.useProgram();

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, pointsBuffer);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
	glEnableVertexAttribArray(1);

	glm::mat4 model	= glm::translate(glm::mat4(1.0f), position) * glm::scale(glm::mat4(1.0f), scale);
	shader.setMat4("model", model);

	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

	glDrawArrays(GL_TRIANGLES, 0, points.size());

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void GlassSupport::setTexture(GLuint texture)
{
	this->texture = texture;
}

void GlassSupport::setPosition(const glm::vec3 & position)
{
	this->position = position;
}

void GlassSupport::setPosition(double x, double y, double z)
{
	position = glm::vec3(x, y, z);
}

void GlassSupport::setScale(const glm::vec3 & scale)
{
	this->scale = scale;
}

void GlassSupport::setScale(double xDimension, double yDimension, double zDimension)
{
	scale = glm::vec3(xDimension, yDimension, zDimension);
}

void GlassSupport::setRotation(const glm::dquat & quat)
{
	this->quat = quat;
}
