#include "Model.h"
#include "OpenGLUtils.h"


#include <iostream>
#include <gtc\matrix_transform.hpp>

Model::Model(const std::string & path, ShaderLoader shader) : shader(shader) {
	loadModel(path);
}
void Model::draw()
{
	shader.useProgram();
	glm::mat4 rotation = glm::mat4_cast(quat);
	glm::mat4 model = glm::translate(glm::mat4(1.0f), position) *
		glm::scale(glm::mat4(1.0f), scale) * rotation;
	shader.setMat4("model", model);
	for (unsigned int i = 0; i < meshes.size(); ++i)
		meshes[i].Draw(shader);
}

void Model::initialize()
{
}

void Model::update()
{
}

void Model::setPosition(const glm::vec3 & position)
{
	this->position = position;
}

void Model::setPosition(float x, float y, float z)
{
	position = glm::vec3(x, y, z);
}

void Model::setScale(const glm::vec3 & scale)
{
	this->scale = scale;
}

void Model::setScale(float xDimension, float yDimension, float zDimension)
{
	scale = glm::vec3(xDimension, yDimension, zDimension);
}

void Model::setRotation(const glm::dquat & quat)
{
	this->quat = quat;
}

void Model::setScaleFactor(float scaleFactor)
{
	this->scaleFactor = scaleFactor;
}

float Model::getScaleFactor() const
{
	return scaleFactor;
}

ShaderLoader & Model::getShader()
{
	return shader;
}

void Model::addDiffuseTexture(unsigned diffuseTexture)
{
	Texture texture(diffuseTexture, "texture_diffuse");
	for (Mesh& mesh : meshes)
		mesh.addTexture(texture);
}

void Model::addSpecularTexture(unsigned specularTexture)
{
	Texture texture(specularTexture, "texture_specular");
	for (Mesh& mesh : meshes)
		mesh.addTexture(texture);
}

void Model::loadModel(const std::string & path)
{
	Assimp::Importer import;
	const aiScene *scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
		return;
	}
	directory = path.substr(0, path.find_last_of('\\'));

	processNode(scene->mRootNode, scene);
}

void Model::processNode(aiNode * node, const aiScene * scene)
{
	for (size_t i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(processMesh(mesh, scene));
	}
	for (size_t i = 0; i < node->mNumChildren; ++i)
		processNode(node->mChildren[i], scene);
}

Mesh Model::processMesh(aiMesh * mesh, const aiScene * scene)
{
	std::vector<Vertex> vertices;
	std::vector<unsigned> indices;
	std::vector<Texture> textures;

	for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
	{
		Vertex vertex;
		glm::vec3 temp;
		temp.x = mesh->mVertices[i].x;
		temp.y = mesh->mVertices[i].y;
		temp.z = mesh->mVertices[i].z;
		vertex.Position = temp;
		if(mesh->HasNormals()) {
			temp.x = mesh->mNormals[i].x;
			temp.y = mesh->mNormals[i].y;
			temp.z = mesh->mNormals[i].z;
		}
		else
			temp = glm::vec3(0.0, 0.0, 0.0);
		vertex.Normal = temp;
		if (mesh->HasTextureCoords(0)) // does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		vertices.push_back(vertex);
	}
	for (size_t i = 0; i < mesh->mNumFaces; ++i)
	{
		aiFace face = mesh->mFaces[i];
		for (size_t j = 0; j < face.mNumIndices; ++j)
			indices.push_back(face.mIndices[j]);
	}
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		std::vector<Texture> diffuseMaps = loadMaterialTextures(material,
			aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		std::vector<Texture> specularMaps = loadMaterialTextures(material,
			aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}

	return Mesh(vertices, indices, textures);
}

std::vector<Texture> Model::loadMaterialTextures(aiMaterial * mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		Texture texture;
		texture.id = OpenGLUtils::generateTexture2D(str.C_Str(), directory);
		texture.type = typeName;
		textures.push_back(texture);
	}
	return textures;
}
